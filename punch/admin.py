from django.contrib import admin
from punch.models import *


# Register your models here.


class Useradmin(admin.ModelAdmin):
    list_display = ['id', 'username', 'iphone', 'email', 'signature','status']
    search_fields = ['username']


class Targetadmin(admin.ModelAdmin):
    list_display = ['id', 'username', 'targetname', 'targetcontent', 'targettype', 'targetstatus']
    search_fields = ['username']

class Punchadmin(admin.ModelAdmin):
    list_display = ['id', 'username', 'punchname', 'punchcontent', 'punchstatus']
    search_fields = ['username']

admin.site.register(User, Useradmin)
admin.site.register(Target, Targetadmin)
admin.site.register(Targetlog)
admin.site.register(Punch, Punchadmin)
admin.site.register(Punchlog)
