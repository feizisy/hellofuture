from django.db import models

"""
用户表
"""


class User(models.Model):
    username = models.CharField(verbose_name="用户名", max_length=20, unique=True, null=True)
    status = models.CharField(verbose_name="用户状态", max_length=4, null=True,help_text="0:已删除；1：使用中",default=1)
    iphone = models.CharField(verbose_name="手机号", max_length=11, unique=True, null=True)
    email = models.EmailField(verbose_name="邮箱", max_length=20, unique=True)
    signature = models.TextField(verbose_name="签名", max_length=500)
    createtime = models.DateTimeField(verbose_name="创建时间", auto_now=True)


def __str__(self):
    return self.realname


"""
计划表
"""


class Target(models.Model):
    username = models.ForeignKey("user",on_delete=models.CASCADE,verbose_name="用户名")
    targetname = models.TextField(verbose_name="计划名称", max_length=200, null=True)
    targetcontent = models.TextField(verbose_name="计划内容", max_length=500, null=True)
    targetstatus = models.CharField(verbose_name="计划状态", max_length=4, null=True,
                                    help_text="0:已过期；1:使用中/未完成；2:已完成；3:已删除")
    targettype = models.CharField(verbose_name="计划类型", max_length=4, null=True,
                                  help_text="0:周计划；1:本月目标；2:今年目标；3：三年目标；4:五年目标；5:终身目标")
    createtime = models.DateTimeField(verbose_name="创建时间", auto_now=True)


def __str__(self):
    return self.realname


"""
打卡表
"""


class Punch(models.Model):
    # punchid = models.AutoField()
    username = models.ForeignKey("user",on_delete=models.CASCADE,verbose_name="用户名")
    punchname = models.TextField(verbose_name="打卡事项", max_length=200, null=True)
    punchcontent = models.TextField(verbose_name="复盘内容", max_length=500, null=True)
    punchstatus = models.CharField(verbose_name="打卡状态", max_length=4, null=True,
                                   help_text="0:已打卡；1:未打卡；2：已过期")
    createtime = models.DateTimeField(verbose_name="创建时间", auto_now=True)

def __str__(self):
    return self.realname


"""
计划日志
"""


class Targetlog(models.Model):
    username = models.ForeignKey("user",on_delete=models.CASCADE,verbose_name="用户名")
    targetname = models.ForeignKey("target",on_delete=models.CASCADE,verbose_name="计划名称")
    targetrecoding = models.TextField(verbose_name="计划操作记录", max_length=500, null=True)
    createtime = models.DateTimeField(verbose_name="创建时间", auto_now=True)


def __str__(self):
    return self.realname


"""
打卡日志
"""


class Punchlog(models.Model):
    username = models.ForeignKey("user",on_delete=models.CASCADE,verbose_name="用户名")
    punchname = models.ForeignKey("punch",on_delete=models.CASCADE,verbose_name="打卡事项")
    punchrecoding = models.TextField(verbose_name="打卡操作记录", max_length=500, null=True)
    createtime = models.DateTimeField(verbose_name="创建时间", auto_now=True)

def __str__(self):
    return self.realname
