# -*-coding=utf-8-*-
from django.contrib import messages
from django.shortcuts import render
from punch.models import User


def index(request):
    try:
        userinfo = User.objects.get(username=request.session.get("username"))
        responsedict = {"userinfo":userinfo}
        return render(request, 'home.html', responsedict)
    except ZeroDivisionError as erro:
        messages.error(request, "用户名或密码错误，请重新登录")
        return render(request, 'home.html')
