from django.contrib import auth, messages
from django.http import HttpResponseRedirect
from django.shortcuts import render
from punch.models import User
from django.contrib.auth.decorators import login_required


def index(request):
    return render(request, "login.html")


def login(request):
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        # authenticate()是auth模块自带的函数，会接收两个参数，正确返回user对象，错误返回None
        user = auth.authenticate(username=username, password=password)

        if user is not None:
            response = HttpResponseRedirect('/home/')
            request.session["username"] = username
            auth.login(request, user)
            return response
        else:
            messages.error(request, "用户名或密码错误，请重新登录")
            return render(request, "login.html", {"error": "用户名或密码错误"})
