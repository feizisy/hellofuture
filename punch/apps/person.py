# -*-coding=utf-8-*-
import json
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from punch.models import User


def index(request):
    try:
        userinfo = User.objects.get(username=request.session.get("username"))
        responsedict = {"userinfo":userinfo}
        return render(request, 'infosetting.html', responsedict)
    except ZeroDivisionError as erro:
        messages.error(request, "获取用户信息失败，请重试")
        return render(request, 'infosetting.html')


def infosetting(request):
    if request.method == "POST":
        try:
            userinfo = User.objects.get(username=request.session.get("username"))
            userinfo.username = request.POST.get("username")
            userinfo.iphone = request.POST.get('iphone')
            userinfo.email = request.POST.get('email')
            userinfo.signature = request.POST.get('signature')
            userinfo.save()
            request.session['username'] = request.POST.get("username")
        except ZeroDivisionError as erro:
            messages.error(request, "更新用户信息失败，请重试")

        response = HttpResponseRedirect('/home/')
        return response
