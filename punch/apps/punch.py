# -*-coding=utf-8-*-
import json
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from punch.models import *


def index(request):
    try:
        return render(request, 'punch.html')

    except ZeroDivisionError as erro:
        messages.error(request, "获取用户信息失败，请重试")
        return render(request, 'punch.html')