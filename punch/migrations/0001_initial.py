# Generated by Django 2.2.6 on 2019-10-29 04:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Punch',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('punchname', models.TextField(max_length=200, null=True, verbose_name='打卡事项')),
                ('punchcontent', models.TextField(max_length=500, null=True, verbose_name='复盘内容')),
                ('punchstatus', models.CharField(help_text='0:已打卡；1:未打卡；2：已过期', max_length=2, null=True, verbose_name='打卡状态')),
                ('createtime', models.DateTimeField(auto_now=True, verbose_name='创建时间')),
            ],
        ),
        migrations.CreateModel(
            name='Target',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('targetname', models.TextField(max_length=200, null=True, verbose_name='计划名称')),
                ('targetcontent', models.TextField(max_length=500, null=True, verbose_name='计划内容')),
                ('targetstatus', models.CharField(help_text='0:已过期；1:使用中/未完成；2:已完成；3:已删除', max_length=2, null=True, verbose_name='计划状态')),
                ('targettype', models.CharField(help_text='0:周计划；1:本月目标；2:今年目标；3：三年目标；4:五年目标；5:终身目标', max_length=2, null=True, verbose_name='计划类型')),
                ('createtime', models.DateTimeField(auto_now=True, verbose_name='创建时间')),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=20, null=True, unique=True, verbose_name='用户名')),
                ('status', models.IntegerField(max_length=2, null=True, verbose_name='用户状态')),
                ('iphone', models.CharField(max_length=11, null=True, unique=True, verbose_name='手机号')),
                ('email', models.EmailField(max_length=20, unique=True, verbose_name='邮箱')),
                ('signature', models.TextField(max_length=500, verbose_name='签名')),
                ('createtime', models.DateTimeField(auto_now=True, verbose_name='创建时间')),
            ],
        ),
        migrations.CreateModel(
            name='Targetlog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('targetrecoding', models.TextField(max_length=500, null=True, verbose_name='计划操作记录')),
                ('createtime', models.DateTimeField(auto_now=True, verbose_name='创建时间')),
                ('targetname', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='punch.Target')),
                ('username', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='punch.User')),
            ],
        ),
        migrations.AddField(
            model_name='target',
            name='username',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='punch.User'),
        ),
        migrations.CreateModel(
            name='Punchlog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('punchrecoding', models.TextField(max_length=500, null=True, verbose_name='打卡操作记录')),
                ('createtime', models.DateTimeField(auto_now=True, verbose_name='创建时间')),
                ('punchname', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='punch.Punch')),
                ('username', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='punch.User')),
            ],
        ),
        migrations.AddField(
            model_name='punch',
            name='username',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='punch.User'),
        ),
    ]
