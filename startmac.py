import os
from common import conf

host = conf.addresses()
ip = host["ip"]
port = host["port"]
paths = os.path.abspath(".")


def starts(ip, port):

    address = ip + ":" + port
    param = os.popen("lsof -i tcp:" + port).read()

    if "LISTEN" in param:
        pid = param.split(" ")[34]
        os.system("kill " + pid)
    else:
        pass
    os.system("python " + paths + "/manage.py runserver " + address)


starts(ip, port)