"""hellofuture URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.urls import path
from django.contrib import admin

from punch.apps import home
from punch.apps import person
from punch.apps import login
from punch.apps import target
from punch.apps import punch

urlpatterns = [
    url(r'^$', login.index),
    url(r'^login/', login.login),
    url(r'^admin/', admin.site.urls),
    url(r'^home/$', home.index),
    url(r'^person/$', person.index),
    url(r'^personinfosetting/$', person.infosetting),
    url(r'^target/$',target.index),
    url(r'^punch/$',punch.index)


]
